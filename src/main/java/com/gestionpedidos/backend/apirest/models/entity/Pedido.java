package com.gestionpedidos.backend.apirest.models.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "pedidos")
public class Pedido implements Serializable {

    private static final long serialVersionUID = -6872476952562703514L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_pedido;
    private Long id_usuario;
    private Long id_cliente;
    private String nombre_cliente;
    private String total_pedido;
    private String id_pago;
    private Date fecha_pedido;
    private Date fecha_creacion;
    private Date fecha_despacho;
    private String numero_despacho;
    private String compania_despacho;
    private Integer folio_pedido;

    public Pedido(Long id_pedido, Long id_usuario, Long id_cliente, String nombre_cliente, String total_pedido, String id_pago, Date fecha_pedido, Date fecha_creacion, Date fecha_despacho, String numero_despacho, String compania_despacho, Integer folio_pedido) {
        this.id_pedido = id_pedido;
        this.id_usuario = id_usuario;
        this.id_cliente = id_cliente;
        this.nombre_cliente = nombre_cliente;
        this.total_pedido = total_pedido;
        this.id_pago = id_pago;
        this.fecha_pedido = fecha_pedido;
        this.fecha_creacion = fecha_creacion;
        this.fecha_despacho = fecha_despacho;
        this.numero_despacho = numero_despacho;
        this.compania_despacho = compania_despacho;
        this.folio_pedido = folio_pedido;
    }

    public Pedido() {

    }

    @PrePersist
    public void prePersist(){
        fecha_creacion = new Date();
    }

    public Integer getFolio_pedido() {
        return folio_pedido;
    }

    public void setFolio_pedido(Integer folio_pedido) {
        this.folio_pedido = folio_pedido;
    }

    public String getNumero_despacho() {
        return numero_despacho;
    }

    public void setNumero_despacho(String numero_despacho) {
        this.numero_despacho = numero_despacho;
    }

    public String getCompania_despacho() {
        return compania_despacho;
    }

    public void setCompania_despacho(String compania_despacho) {
        this.compania_despacho = compania_despacho;
    }

    public Long getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Long id_usuario) {
        this.id_usuario = id_usuario;
    }

    public Long getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Long id_cliente) {
        this.id_cliente = id_cliente;
    }

    public Date getFecha_pedido() {
        return fecha_pedido;
    }

    public void setFecha_pedido(Date fecha_pedido) {
        this.fecha_pedido = fecha_pedido;
    }

    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public Date getFecha_despacho() {
        return fecha_despacho;
    }

    public void setFecha_despacho(Date fecha_despacho) {
        this.fecha_despacho = fecha_despacho;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(Long id_pedido) {
        this.id_pedido = id_pedido;
    }


    public String getNombre_cliente() {
        return nombre_cliente;
    }

    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }

    public String getTotal_pedido() {
        return total_pedido;
    }

    public void setTotal_pedido(String total_pedido) {
        this.total_pedido = total_pedido;
    }

    public String getId_pago() {
        return id_pago;
    }

    public void setId_pago(String id_pago) {
        this.id_pago = id_pago;
    }
}
