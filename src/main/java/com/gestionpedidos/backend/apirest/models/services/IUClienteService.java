package com.gestionpedidos.backend.apirest.models.services;

import com.gestionpedidos.backend.apirest.models.entity.Cliente;
import com.gestionpedidos.backend.apirest.models.entity.Usuario;

import java.util.List;

public interface IUClienteService {

    public List<Cliente> findAll();

    public Cliente findById(Long id);

    public Cliente save(Cliente cliente);

    public void delete(Long id);

    public List<Usuario> getNegocios();
}
