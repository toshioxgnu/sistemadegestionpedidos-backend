package com.gestionpedidos.backend.apirest.models.services;

import com.gestionpedidos.backend.apirest.classes.LoginForm;
import com.gestionpedidos.backend.apirest.models.entity.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IUsuarioService {

    public List<Usuario> findAll();

    public Usuario findById(Long id);

    public Usuario save(Usuario usuario);

    public void delete(Long id);

    public Usuario findByEmailAndPassword(LoginForm login);
}
