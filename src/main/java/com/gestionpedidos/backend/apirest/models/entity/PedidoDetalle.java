package com.gestionpedidos.backend.apirest.models.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="pedido_detalle")
public class PedidoDetalle implements Serializable {

    private static final long serialVersionUID = 8340843078615083623L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_pedidodetalle;
    private Long id_pedido;
    private String nombre_producto;
    private Integer linea;
    private Integer cantidad;
    private Integer precio;
    private Integer total_linea;

    public PedidoDetalle(Long id_pedidodetalle, Long id_pedido, String nombre_producto, Integer linea, Integer cantidad, Integer precio, Integer total_linea) {
        this.id_pedidodetalle = id_pedidodetalle;
        this.id_pedido = id_pedido;
        this.nombre_producto = nombre_producto;
        this.linea = linea;
        this.cantidad = cantidad;
        this.precio = precio;
        this.total_linea = total_linea;
    }

    public PedidoDetalle(){}

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId_pedidodetalle() {
        return id_pedidodetalle;
    }

    public void setId_pedidodetalle(Long id_pedidodetalle) {
        this.id_pedidodetalle = id_pedidodetalle;
    }

    public Long getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(Long id_pedido) {
        this.id_pedido = id_pedido;
    }

    public String getNombre_producto() {
        return nombre_producto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    public Integer getLinea() {
        return linea;
    }

    public void setLinea(Integer linea) {
        this.linea = linea;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    public Integer getTotal_linea() {
        return total_linea;
    }

    public void setTotal_linea(Integer total_linea) {
        this.total_linea = total_linea;
    }
}
