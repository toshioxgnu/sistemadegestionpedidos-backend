package com.gestionpedidos.backend.apirest.models.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="carrito")
public class Carrito implements Serializable {

    private static final long serialVersionUID = -6675762368434873684L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_carrito;
    private Long id_usuario;
    private Long id_negocio;
    private Long id_prod;
    private Date fecha_creacion;
    private Date fecha_update;
    private Float total_linea;
    private Integer cantidad;

    @PrePersist
    public void prePersist() {
        fecha_creacion = new Date();
    }

    public Carrito() {
    }

    public Carrito(Long id_carrito, Long id_usuario, Long id_negocio, Long id_prod, Date fecha_creacion, Date fecha_update, Float total_linea, Integer cantidad) {
        this.id_carrito = id_carrito;
        this.id_usuario = id_usuario;
        this.id_negocio = id_negocio;
        this.id_prod = id_prod;
        this.fecha_creacion = fecha_creacion;
        this.fecha_update = fecha_update;
        this.total_linea = total_linea;
        this.cantidad = cantidad;
    }

    public Long getId_negocio() {
        return id_negocio;
    }

    public void setId_negocio(Long id_negocio) {
        this.id_negocio = id_negocio;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId_carrito() {
        return id_carrito;
    }

    public void setId_carrito(Long id_carrito) {
        this.id_carrito = id_carrito;
    }

    public Long getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Long id_usuario) {
        this.id_usuario = id_usuario;
    }

    public Long getId_prod() {
        return id_prod;
    }

    public void setId_prod(Long id_prod) {
        this.id_prod = id_prod;
    }

    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public Date getFecha_update() {
        return fecha_update;
    }

    public void setFecha_update(Date fecha_update) {
        this.fecha_update = fecha_update;
    }

    public Float getTotal_linea() {
        return total_linea;
    }

    public void setTotal_linea(Float total_linea) {
        this.total_linea = total_linea;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

}
