package com.gestionpedidos.backend.apirest.models.dao;

import com.gestionpedidos.backend.apirest.models.entity.Carrito;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IUCarritoDao extends CrudRepository<Carrito, Long> {

    @Query(value = "select c from Carrito c join Usuario u on c.id_usuario=u.id_usuario join Producto p on c.id_usuario=p.id_usuario where c.id_usuario = :#{#id_usuario} ")
    public List<Carrito> findByIdUsuario(@Param("id_usuario") Long id_usuario);

    @Query(value = "delete from Carrito c where c.id_usuario = :#{#id_usuario} ")
    public void deleteByIdUsuario(@Param("id_usuario") Long id_usuario);

}
