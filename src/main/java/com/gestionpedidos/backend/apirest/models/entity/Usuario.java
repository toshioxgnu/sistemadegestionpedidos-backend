package com.gestionpedidos.backend.apirest.models.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "usuario")
public class Usuario implements Serializable {

    private static final long serialVersionUID = 8517806300207427823L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_usuario;
    @Column(insertable = false, updatable = false)
    private String dtype;
    private String nombre;
    private String apellido;
    private String email;
    private String password;
    private String nombreusuario;
    private Date fecha_creacion;
    private Date fecha_update;

    public Usuario(Long id_usuario, String dtype, String nombre, String apellido, String email, String password, String nombreusuario, Date fecha_creacion, Date fecha_update) {
        this.id_usuario = id_usuario;
        this.dtype = dtype;
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.password = password;
        this.nombreusuario = nombreusuario;
        this.fecha_creacion = fecha_creacion;
        this.fecha_update = fecha_update;
    }

    public Usuario() {
    }

    @PrePersist
    public void prePersist() {
        fecha_creacion = new Date();
        fecha_update = new Date();
    }

    public String getDtype() {
        return dtype;
    }

    public void setDtype(String dtype) {
        this.dtype = dtype;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Date getFecha_update() {
        return fecha_update;
    }

    public void setFecha_update(Date fecha_update) {
        this.fecha_update = fecha_update;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombreusuario() {
        return nombreusuario;
    }

    public void setNombreusuario(String nombreusuario) {
        this.nombreusuario = nombreusuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public Long getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Long id_usuario) {
        this.id_usuario = id_usuario;
    }
}
