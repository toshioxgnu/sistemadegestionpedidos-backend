package com.gestionpedidos.backend.apirest.models.dao;

import com.gestionpedidos.backend.apirest.classes.LoginForm;
import com.gestionpedidos.backend.apirest.models.entity.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface IUsuarioDao extends CrudRepository<Usuario, Long> {

    @Query(value = "select u from Usuario u  where u.email = :#{#login.email} and u.password= :#{#login.password}")
    Usuario findByEmailAndPassword(@Param("login") LoginForm login);

}
