package com.gestionpedidos.backend.apirest.models.services;

import com.gestionpedidos.backend.apirest.models.dao.IUClienteDao;
import com.gestionpedidos.backend.apirest.models.entity.Cliente;
import com.gestionpedidos.backend.apirest.models.entity.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ClienteServiceImpl implements IUClienteService {

    @Autowired
    private IUClienteDao clienteDao;

    @Override
    @Transactional
    public List<Cliente> findAll() {
        return (List<Cliente>) clienteDao.findAll();
    }

    @Override
    public Cliente findById(Long id) {
        return clienteDao.findById(id).orElse(null);
    }

    @Override
    public Cliente save(Cliente cliente) {
        return clienteDao.save(cliente);
    }

    @Override
    public void delete(Long id) {
        clienteDao.deleteById(id);
    }

    @Override
    public List<Usuario> getNegocios(){ return clienteDao.getNegocios(); }
}
