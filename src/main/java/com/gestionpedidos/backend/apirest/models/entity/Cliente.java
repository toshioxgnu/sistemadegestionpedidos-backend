package com.gestionpedidos.backend.apirest.models.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "cliente")
public class Cliente extends Usuario implements Serializable {

    private static final long serialVersionUID = 6019139291525837453L;
    private String direccion;
    private Integer region;
    private String comuna;
    private Long id_Pago;

    public Cliente(Long id_usuario, String dtype, String nombre, String apellido, String email, String password, String nombreusuario, Date fecha_creacion, Date fecha_update, String direccion, Integer region, String comuna, Long id_Pago) {
        super(id_usuario, dtype, nombre, apellido, email, password, nombreusuario, fecha_creacion, fecha_update);
        this.direccion = direccion;
        this.region = region;
        this.comuna = comuna;
        this.id_Pago = id_Pago;
    }

    public Cliente(String direccion, Integer region, String comuna, Long id_Pago) {
        this.direccion = direccion;
        this.region = region;
        this.comuna = comuna;
        this.id_Pago = id_Pago;
    }

    public Cliente() {

    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getRegion() {
        return region;
    }

    public void setRegion(Integer region) {
        this.region = region;
    }

    public String getComuna() {
        return comuna;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

    public Long getId_Pago() {
        return id_Pago;
    }

    public void setId_Pago(Long id_Pago) {
        this.id_Pago = id_Pago;
    }
}
