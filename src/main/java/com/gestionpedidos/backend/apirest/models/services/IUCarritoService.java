package com.gestionpedidos.backend.apirest.models.services;

import com.gestionpedidos.backend.apirest.models.entity.Carrito;

import java.util.List;

public interface IUCarritoService {

    public List<Carrito> findByIdUsuario(Long id_usuario);

    public Carrito save(Carrito carrito);

    public void deletebyIDUsuario(Long id_usuario);
}
