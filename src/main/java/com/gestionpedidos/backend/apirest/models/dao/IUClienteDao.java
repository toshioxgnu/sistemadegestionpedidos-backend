package com.gestionpedidos.backend.apirest.models.dao;

import com.gestionpedidos.backend.apirest.models.entity.Cliente;
import com.gestionpedidos.backend.apirest.models.entity.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IUClienteDao extends CrudRepository<Cliente, Long> {

    @Query("select u from Usuario u where u.dtype = 'Usuario'")
    public List<Usuario> getNegocios();
}
