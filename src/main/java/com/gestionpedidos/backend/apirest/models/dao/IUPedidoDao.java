package com.gestionpedidos.backend.apirest.models.dao;

import com.gestionpedidos.backend.apirest.models.entity.Pedido;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IUPedidoDao extends CrudRepository<Pedido, Long> {

    @Query(value = "select p from Pedido p where p.id_cliente = :#{#id_usuario}")
    public List<Pedido> findByIdUsuario( @Param("id_usuario") Long id_usuario);
}
