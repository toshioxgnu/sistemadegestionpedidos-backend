package com.gestionpedidos.backend.apirest.models.services;

import com.gestionpedidos.backend.apirest.models.entity.Pedido;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IUPedidoService {

    public List<Pedido> findByIdUsuario(Long id_usuario);

    public Pedido save(Pedido pedido);

    public Pedido findById(Long id_pedido);

    public void delete(Long id_pedido);
    
}
