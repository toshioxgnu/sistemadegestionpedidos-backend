package com.gestionpedidos.backend.apirest.models.services;

import com.gestionpedidos.backend.apirest.models.dao.IUProductoDao;
import com.gestionpedidos.backend.apirest.models.entity.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductoServiceImpl implements IUProductoService {

    @Autowired
    private IUProductoDao productodao;

    @Override
    public List<Producto> getProductosByIdUsuario(Long id_usuario) {
        return productodao.getProductosByIdUsuario(id_usuario);
    }

    @Override
    public Producto save(Producto producto) {
        return productodao.save(producto);
    }

    @Override
    public void delete(Long id_producto) {

    }

    @Override
    public Producto findById(Long id){
        return productodao.findById(id).orElse(null);
    }
}
