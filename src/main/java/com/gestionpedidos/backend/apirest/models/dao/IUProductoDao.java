package com.gestionpedidos.backend.apirest.models.dao;

import com.gestionpedidos.backend.apirest.models.entity.Producto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IUProductoDao extends CrudRepository<Producto, Long> {

    @Query(value = "select p from  Producto p where p.id_usuario = :id_usuario")
    public List<Producto> getProductosByIdUsuario(Long id_usuario);

}
