package com.gestionpedidos.backend.apirest.models.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="producto")
public class Producto implements Serializable {

     private static final long serialVersionUID = -3447623915100345891L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_producto;
    @Column(columnDefinition = "varchar(255) default null")
    private String cod_producto;
    private String nombre_producto;
    @Column(columnDefinition = "varchar(255) default null")
    private String marca_producto;
    private Long id_usuario;
    private Date creation_date;
    private Date update_date;
    @Column(columnDefinition = "varchar(255) default 0")
    private String valor;

    public Producto(Long id_producto, String cod_producto, String nombre_producto, String marca_producto, Long id_usuario, Date creation_date, Date update_date, String valor) {
        this.id_producto = id_producto;
        this.cod_producto = cod_producto;
        this.nombre_producto = nombre_producto;
        this.marca_producto = marca_producto;
        this.id_usuario = id_usuario;
        this.creation_date = creation_date;
        this.update_date = update_date;
        this.valor = valor;
    }

    public Producto() {

    }

    @PrePersist
    public void prePersist(){
        creation_date = new Date();
        update_date = new Date();
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId_producto() {
        return id_producto;
    }

    public void setId_producto(Long id_producto) {
        this.id_producto = id_producto;
    }

    public String getCod_producto() {
        return cod_producto;
    }

    public void setCod_producto(String cod_producto) {
        this.cod_producto = cod_producto;
    }

    public String getNombre_producto() {
        return nombre_producto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    public String getMarca_producto() {
        return marca_producto;
    }

    public void setMarca_producto(String marca_producto) {
        this.marca_producto = marca_producto;
    }

    public Long getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Long id_usuario) {
        this.id_usuario = id_usuario;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }
}
