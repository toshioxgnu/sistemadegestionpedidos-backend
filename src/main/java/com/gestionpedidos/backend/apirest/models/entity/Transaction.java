package com.gestionpedidos.backend.apirest.models.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "transactions")
public class Transaction implements Serializable {

    private static final long serialVersionUID = 2809253900880174579L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_transaction;
    private Long id_usuario;
    private Long id_creditcard;
    private String id_paypaltransaction;
    private String response_transaction;
    private String pathpdf;
    private Date transaction_date;

    @PrePersist
    public void prePersist() {
        transaction_date = new Date();
    }

    public Transaction(Long id_transaction, Long id_usuario, Long id_creditcard, String id_paypaltransaction, String response_transaction, String pathpdf, Date transaction_date) {
        this.id_transaction = id_transaction;
        this.id_usuario = id_usuario;
        this.id_creditcard = id_creditcard;
        this.id_paypaltransaction = id_paypaltransaction;
        this.response_transaction = response_transaction;
        this.pathpdf = pathpdf;
        this.transaction_date = transaction_date;
    }

    public Transaction() {
    }

    public Date getTransaction_date() {
        return transaction_date;
    }

    public void setTransaction_date(Date transaction_date) {
        this.transaction_date = transaction_date;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId_transaction() {
        return id_transaction;
    }

    public void setId_transaction(Long id_transaction) {
        this.id_transaction = id_transaction;
    }

    public Long getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Long id_usuario) {
        this.id_usuario = id_usuario;
    }

    public Long getId_creditcard() {
        return id_creditcard;
    }

    public void setId_creditcard(Long id_creditcard) {
        this.id_creditcard = id_creditcard;
    }

    public String getId_paypaltransaction() {
        return id_paypaltransaction;
    }

    public void setId_paypaltransaction(String id_paypaltransaction) {
        this.id_paypaltransaction = id_paypaltransaction;
    }

    public String getResponse_transaction() {
        return response_transaction;
    }

    public void setResponse_transaction(String response_transaction) {
        this.response_transaction = response_transaction;
    }

    public String getPathpdf() {
        return pathpdf;
    }

    public void setPathpdf(String pathpdf) {
        this.pathpdf = pathpdf;
    }
}
