package com.gestionpedidos.backend.apirest.models.services;

import com.gestionpedidos.backend.apirest.models.dao.IUCarritoDao;
import com.gestionpedidos.backend.apirest.models.entity.Carrito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CarritoServiceimpl implements IUCarritoService {

    @Autowired
    private IUCarritoDao carritodao;

    @Override
    @Transactional
    public List<Carrito> findByIdUsuario(Long id_usuario){
        return carritodao.findByIdUsuario(id_usuario);
    }

    @Override
    public Carrito save(Carrito carrito) {
        return carritodao.save(carrito);
    }

    @Override
    public void deletebyIDUsuario(Long id_usuario) {
        carritodao.deleteByIdUsuario(id_usuario);
    }
}
