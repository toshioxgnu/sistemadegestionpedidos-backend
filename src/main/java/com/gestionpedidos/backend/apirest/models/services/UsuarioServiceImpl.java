package com.gestionpedidos.backend.apirest.models.services;

import com.gestionpedidos.backend.apirest.classes.LoginForm;
import com.gestionpedidos.backend.apirest.models.dao.IUsuarioDao;
import com.gestionpedidos.backend.apirest.models.entity.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

    @Autowired
    private IUsuarioDao usuarioDao;

    @Override
    @Transactional
    public List<Usuario> findAll() {
        return (List<Usuario>) usuarioDao.findAll();
    }

    @Override
    public Usuario findById(Long id) {
        return usuarioDao.findById(id).orElse(null);
    }

    @Override
    public Usuario save(Usuario usuario) {
        return usuarioDao.save(usuario);
    }

    @Override
    public void delete(Long id) {
        usuarioDao.deleteById(id);
    }

    @Override
    public Usuario findByEmailAndPassword(LoginForm login) {
        return usuarioDao.findByEmailAndPassword(login);
    }
}
