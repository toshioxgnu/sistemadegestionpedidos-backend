package com.gestionpedidos.backend.apirest.models.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "creditCard")
public class CreditCard implements Serializable {

    private static final long serialVersionUID = -7486043054503269493L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_creditcard;
    private Long id_usuario;
    private Long card_number;
    private String first_name;
    private String last_name;
    private Date expire_date;
    private Long ccv;
    private String card_type;
    private Date card_creation_date;

    @PrePersist
    public void prePersist() {
        card_creation_date = new Date();
    }

    public CreditCard(Long id_creditcard, Long id_usuario, Long card_number, String first_name, String last_name, Date expire_date, Long ccv, String card_type, Date card_creation_date) {
        this.id_creditcard = id_creditcard;
        this.id_usuario = id_usuario;
        this.card_number = card_number;
        this.first_name = first_name;
        this.last_name = last_name;
        this.expire_date = expire_date;
        this.ccv = ccv;
        this.card_type = card_type;
        this.card_creation_date = card_creation_date;
    }

    public CreditCard() {
    }

    public Date getCard_creation_date() {
        return card_creation_date;
    }

    public void setCard_creation_date(Date card_creation_date) {
        this.card_creation_date = card_creation_date;
    }

    public String getCard_type() {
        return card_type;
    }

    public void setCard_type(String card_type) {
        this.card_type = card_type;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId_creditcard() {
        return id_creditcard;
    }

    public void setId_creditcard(Long id_creditcard) {
        this.id_creditcard = id_creditcard;
    }

    public Long getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Long id_usuario) {
        this.id_usuario = id_usuario;
    }

    public Long getCard_number() {
        return card_number;
    }

    public void setCard_number(Long card_number) {
        this.card_number = card_number;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public Date getExpire_date() {
        return expire_date;
    }

    public void setExpire_date(Date expire_date) {
        this.expire_date = expire_date;
    }

    public Long getCcv() {
        return ccv;
    }

    public void setCcv(Long ccv) {
        this.ccv = ccv;
    }
}
