package com.gestionpedidos.backend.apirest.models.services;

import com.gestionpedidos.backend.apirest.models.dao.IUPedidoDao;
import com.gestionpedidos.backend.apirest.models.entity.Pedido;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PedidoServiceImpl implements IUPedidoService {

    @Autowired
    private IUPedidoDao pedidodao;

    @Override
    public List<Pedido> findByIdUsuario(Long id_usuario) {
        return pedidodao.findByIdUsuario(id_usuario);
    }

    @Override
    public Pedido save(Pedido pedido) {
        return pedidodao.save(pedido);
    }

    @Override
    public Pedido findById(Long id_pedido) {
        return pedidodao.findById(id_pedido).orElse(null);
    }

    @Override
    public void delete(Long id_pedido) {
        pedidodao.deleteById(id_pedido);
    }
}
