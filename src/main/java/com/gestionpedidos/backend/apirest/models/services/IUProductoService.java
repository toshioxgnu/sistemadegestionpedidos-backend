package com.gestionpedidos.backend.apirest.models.services;

import com.gestionpedidos.backend.apirest.models.entity.Producto;

import java.util.List;

public interface IUProductoService {

    public List<Producto> getProductosByIdUsuario(Long id_usuario);

    public Producto save(Producto producto);

    public void delete(Long id_producto);

    public Producto findById(Long id);
}
