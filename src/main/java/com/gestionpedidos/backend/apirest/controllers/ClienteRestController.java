package com.gestionpedidos.backend.apirest.controllers;

import com.gestionpedidos.backend.apirest.models.entity.Cliente;
import com.gestionpedidos.backend.apirest.models.entity.Usuario;
import com.gestionpedidos.backend.apirest.models.services.IUClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/api")
public class ClienteRestController {

    @Autowired
    private IUClienteService clienteService;

    @PostMapping("/clientes")
    public List<Cliente> cliente(){
        return clienteService.findAll();
    }

    @PostMapping("/clientes/{id}")
    public Cliente getClientexId(@PathVariable Long id){
        return clienteService.findById(id);
    }

    @PostMapping("/clientes/new")
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente guardaCliente(@RequestBody Cliente cliente){
        return clienteService.save(cliente);
    }

    @PostMapping("/clientes/getNegocios")
    public List<Usuario> getUsuariosNegocio(){
        return clienteService.getNegocios();
    }

}
