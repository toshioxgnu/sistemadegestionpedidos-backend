package com.gestionpedidos.backend.apirest.controllers;

import com.gestionpedidos.backend.apirest.models.entity.Producto;
import com.gestionpedidos.backend.apirest.models.services.IUProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
@RequestMapping("/api")
public class ProductoRestController {

    @Autowired
    private IUProductoService iuproductosservice;

    @PostMapping("/productos/new")
    @ResponseStatus(HttpStatus.CREATED)
    public Producto save(@RequestBody Producto producto) {
        return iuproductosservice.save(producto);
    }

    @PostMapping("/productos/{id_usuario}")
    public List<Producto> getByIdCliente(@PathVariable Long id_usuario) {
        return iuproductosservice.getProductosByIdUsuario(id_usuario);
    }

    @PostMapping("/productoborra/{idProducto}")
    public void delete(Long idProducto){
        iuproductosservice.delete(idProducto);
    }

    @PostMapping("/producto/{id}")
    public Producto findById(@PathVariable Long id ){
        return iuproductosservice.findById(id);
    }

}
