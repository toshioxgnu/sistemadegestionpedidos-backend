package com.gestionpedidos.backend.apirest.controllers;

import com.gestionpedidos.backend.apirest.models.entity.Carrito;
import com.gestionpedidos.backend.apirest.models.services.IUCarritoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/api")
public class CarritoRestController {

    @Autowired
    public IUCarritoService carritoService;

    @PostMapping("/carrito/{id_usuario}")
    public List<Carrito> findByIdUsuario(@PathVariable Long id_usuario){
        return carritoService.findByIdUsuario(id_usuario);
    }

    @PostMapping("/carrito/new")
    @ResponseStatus(HttpStatus.CREATED)
    public Carrito save(@RequestBody Carrito carrito){
        return carritoService.save(carrito);
    }

    @PostMapping("/carrito/delete/{id_usuario}")
    public void deletebyIdUsuario(@PathVariable Long id_usuario){
        carritoService.deletebyIDUsuario(id_usuario);
    }

}
