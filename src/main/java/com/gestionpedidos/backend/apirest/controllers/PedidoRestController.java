package com.gestionpedidos.backend.apirest.controllers;

import com.gestionpedidos.backend.apirest.models.entity.Pedido;
import com.gestionpedidos.backend.apirest.models.services.IUPedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/api")
public class PedidoRestController {

    @Autowired
    private IUPedidoService iupedidoservice;

    @PostMapping("/pedidos/{id_usuario}")
    public List<Pedido> findByIdUsuario(@PathVariable Long id_usuario) {
        return iupedidoservice.findByIdUsuario(id_usuario);
    }

    @PostMapping("/pedidos/save")
    @ResponseStatus(HttpStatus.CREATED)
    public Pedido save(@RequestBody Pedido pedido) {
        return iupedidoservice.save(pedido);
    }

    @PostMapping("/pedido/{id_pedido}")
    public Pedido GetById(@PathVariable Long id_pedido) {
        return iupedidoservice.findById(id_pedido);
    }

    @PostMapping("/pedidoborra/{id_pedido}")
    @ResponseStatus(HttpStatus.GONE)
    public void Delete(@PathVariable Long id_pedido) {
        iupedidoservice.delete(id_pedido);
    }

}
