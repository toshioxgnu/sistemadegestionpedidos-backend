package com.gestionpedidos.backend.apirest.controllers;

import com.gestionpedidos.backend.apirest.classes.LoginForm;
import com.gestionpedidos.backend.apirest.models.entity.Usuario;
import com.gestionpedidos.backend.apirest.models.services.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/api")
public class UsuarioRestController {

    @Autowired
    private IUsuarioService usuarioService;

    @PostMapping("/usuarios")
    public List<Usuario> index(){
        return usuarioService.findAll();
    }

    @PostMapping("/usuarios/{id}")
    public Usuario show(@PathVariable Long id){
        return usuarioService.findById(id);
    }

    @PostMapping("/usuarios/new")
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario create(@RequestBody Usuario usuario){
        return usuarioService.save(usuario);
    }

    @PostMapping("/usuarios/login")
    public Usuario findByEmailAndPassword(@RequestBody LoginForm login){
        return usuarioService.findByEmailAndPassword(login);
    }

}
